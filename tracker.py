import time
import rtmidi
from rtmidi.midiconstants import CONTROLLER_CHANGE
import socket
import struct
import math

sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
sock.bind(("0.0.0.0", 5555))

midiout = rtmidi.MidiOut()
available_ports = midiout.get_ports()
last_rot = 0

if available_ports:
    midiout.open_port(0)
else:
    midiout.open_virtual_port("My virtual output")

while 1:
    data,_ = sock.recvfrom(128)
    #print("len", len(data))
    # based on https://github.com/opentrack/opentrack/commit/8e23875fad07a60539a2cf0bf0e364c96cc35dde 
    unpacked = struct.unpack_from("=Hfff", data)
    rot = round((unpacked[1] + math.pi) / (2*math.pi) * 127)
    if(rot != last_rot):
        print(rot)
        last_rot = rot
        midi_cmd = [CONTROLLER_CHANGE, 3, 127-rot]
        midiout.send_message(midi_cmd)

del midiout

