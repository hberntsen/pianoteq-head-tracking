# Pianoteq head tracking

Automated head rotation input from an Android phone to Pianoteq for binaural
audio. It receives the head rotation from the Android phone via UDP messages
and translates them to MIDI commands on a virtual MIDI device. Pianoteq can
subsequently process the commands as head rotation.

** Unfortunately, Pianoteq cannot smoothly handle the head rotation changes and
therefore your audio will be choppy. This is a known issue:
https://www.forum-pianoteq.com/viewtopic.php?id=621 **

## Requirements

This was tested with a Galaxy S4 running Androig Pie (LineageOS) and Xubuntu
18.04. The Pianoteq 6.5.4 Standard (trial) version was used for this experiment.

## Setup

1. Install the FreePIE IMU sender app. It's not in the app store but the
   opentrack project distributes an apk:
   https://github.com/opentrack/opentrack/blob/192e1131873e66d2118afd7a6e13e5701053a4d0/contrib/freepie-udp/com.freepie.android.imu.apk.
   The source seems to be available at
   https://github.com/AndersMalmgren/FreePIE/tree/837209069f74bbf01d2246d937f13874c2f4c8e2/Lib/Android
   .
2. Open the app, point it to your machine running Pianoteq. Only send
   orientation: ![FrieePIE IMU Screenshot](freepie.png)
3. Run the `tracker.py` from this repo. It should print numbers when your phone
   rotates.
4. Open Pianoteq. In the options, ensure that the `RtMidi output` is activated
   in the Devices tab. On the MIDI tab, add a mapping from controller 3 to the
   Head Angle: 

   ![Pianoteq screenshot](pianoteq-midi.png).
   
   You should see the rotation events in the interface.
   
5. You're all set up. The microphone configuration screen in Pianoteq should
   now show your heaed rotations.
